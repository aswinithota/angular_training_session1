let v1 = 6;
console.log(`v1=${v1} and data type is ${typeof(v1)}`);

let v2 = "Micky";
console.log(`v2=${v2} and data type is ${typeof(v2)}`);

let v3 = true;
console.log(`v3=${v3} and data type is ${typeof(v3)}`);

let v4 = {};
console.log(`v4=${v4} and data type is ${typeof(v4)}`);

let v5 = [];
console.log(`v5=${v5} and data type is ${typeof(v5)}`);

let v6 = [];
console.log(`v6=${v6} and data type is ${typeof(v6)}`);

let v7 = 20.24 ;
console.log(`v7=${v7} and data type is ${typeof(v7)}`);

let date = new Date();
let msg = date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear();
console.log(`Date is ${msg}`)

msg = date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
console.log(`Time is ${msg}`);
