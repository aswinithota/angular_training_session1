"use strict";
exports.__esModule = true;
var Numbers = /** @class */ (function () {
    function Numbers(a, b) {
        this.a = a;
        this.b = b;
    }
    Numbers.prototype.getNumbers = function () {
        console.log("a=" + this.a + ",b=" + this.b);
    };
    return Numbers;
}());
exports.Numbers = Numbers;
