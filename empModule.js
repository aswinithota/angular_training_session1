var Employees = /** @class */ (function () {
    function Employees() {
    }
    Employees.prototype.setDetails = function (id, name, job) {
        this.id = id;
        this.name = name;
        this.job = job;
    };
    Employees.prototype.getDetails = function () {
        console.log("id: " + this.id + ",emp name : " + this.name + ",job : " + this.job);
    };
    return Employees;
}());
var emp1 = new Employees;
emp1.setDetails(1, "Ashu", "Associate Consultant");
emp1.getDetails();
