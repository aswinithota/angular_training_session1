let item1 = [];
let item2 = new Array();
console.log(`${typeof(item1)}`);
console.log(`${typeof(item2)}`);

console.log("========================================");

let item3 = [10,20,30,40,50];
for(let i = 0; i < item3.length;i++){
    console.log(item3[i]);
}

console.log("========================================");

let item4 = ["ashu","micky","minnie"];
for(let i in item4){
    console.log(item4[i])
}

console.log("========================================");

let item5 = ["ashu",5,true,{designation:"associate Consultant"},["ashu","micky"]];
for(let i of item5){
    console.log(i);
}