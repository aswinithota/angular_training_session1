class Employees{
    private id:number;
    private name:string;
    private job:string;
    public setDetails(id:number,name:string,job:string){
        this.id = id;
        this.name = name;
        this.job = job;
    }
    public getDetails(){
        console.log(`id: ${this.id},emp name : ${this.name},job : ${this.job}`)
    }
}

let emp1 = new Employees;
emp1.setDetails(1,"Ashu","Associate Consultant");
emp1.getDetails();